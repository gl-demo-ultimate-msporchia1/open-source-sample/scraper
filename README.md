# Project settings report

Create report of project attributes using the GitLab API and GitLab pages.

## Features

- Queries all projects in a group or alternatively, all specified projects in config.yml
- Retrieves all project attributes from https://docs.gitlab.com/ee/api/projects.html#get-single-project
- Produces HTML CSV

## Usage

`python3 projects_report.py $GIT_TOKEN $CONFIG_YML`

## Configuration

- Export / fork repository.
- edit config.yml
  - specify GitLab URL and groups or projects using their ID.
  - If groups are used, all projects in the group are retrieved instead of individual projects.
- Make sure Settings -> Permissions -> Pages is limited to project members
- Run the Pipeline to get your report
- access using pages
