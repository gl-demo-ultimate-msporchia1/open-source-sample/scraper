#!/usr/bin/env python3

import gitlab
import csv
import argparse
import yaml
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

from mako.template import Template

def write_csv(project_objects, fields):
    file_name = "projects_report.csv"
    with open(file_name,"w") as report:
        reportwriter = csv.writer(report, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        reportwriter.writerow(fields)
        for project in project_objects:
            row = []
            for field in fields:
                if field in project:
                    row.append(project[field])
                else:
                    row.append("")
            reportwriter.writerow(row)


def get_project_settings(gl, projects):
    project_objects = []
    for project in projects:
        project_object = gl.projects.get(project)
        # remove runner token to prevent leakage
        project_attributes = project_object.attributes
        remove_runner_token = project_attributes.pop('runners_token', None)
        if remove_runner_token:
            project_objects.append(project_attributes)
    return project_objects

def get_group_projects(gl, groups):
    projects = []
    for topgroup in groups:
        group = gl.groups.get(topgroup)
        group_projects = group.projects.list(as_list=False, include_subgroups=True)
        for group_project in group_projects:
            projects.append(group_project.attributes["id"])
    return projects

def get_project_mrs(gl, projects):
    mrs = []
    for project in projects:
        gl_project = gl.projects.get(project)
        project_mrs = gl_project.mergerequests.list(state="opened", as_list=False)

        for mr in project_mrs:
            # page = requests.get(mr.attributes['web_url'],headers={'PRIVATE-TOKEN': args.token})

            # soup = BeautifulSoup(page.text, parser='html.parser')
            # ul_element = soup.select('//*[@id="widget-state"]/div[2]/section/div[2]/div/ul')
            options = webdriver.ChromeOptions()
            options.add_argument("--headless=new")
            driver = webdriver.Chrome(options=options)
            driver.implicitly_wait(30)
            driver.get(mr.attributes['web_url'])
            ul_dependson = driver.find_element(By.XPATH, '//*[@id="widget-state"]/div[2]/section/div[2]/div/ul')
            a_elements = ul_dependson.find_elements(By.TAG_NAME, 'a')

            depends_on = []
            for a in a_elements:
                if a.get_attribute('class') == 'gl-link sortable-link':
                    href_value = a.get_attribute('href')
                    depends_on.append(href_value)

            mrs.append({
                'project': project,
                'mr_id': mr.attributes['iid'],
                'mr_title': mr.attributes['title'],
                'depends_on': depends_on
            })
        
    return mrs

parser = argparse.ArgumentParser(description='Create report for MR with MR Dependencies')
parser.add_argument('--token', help='API token able to read the requested projects')
parser.add_argument('--configfile', help='CSV file that defines requested projects')
parser.add_argument('--gitlaburl', help='Your GitLab Instance URL (e.g. https://gitlab.com)',required=False)
args = parser.parse_args()

gitlaburl = (args.gitlaburl if args.gitlaburl is not None else "https://gitlab.com/")
gl = gitlab.Gitlab(gitlaburl, private_token=args.token)

configfile = args.configfile
groups = []
projects = []
fields = []
with open(configfile, "r") as c:
    config = yaml.load(c, Loader=yaml.FullLoader)
    gitlaburl = config["gitlab_url"] if config["gitlab_url"].endswith("/") else config["gitlab_url"] + "/"
    gl = gitlab.Gitlab(gitlaburl, private_token=args.token)

    if "groups" in config:
        groups = config["groups"]
        projects = get_group_projects(gl, groups)
    else:
        if not "projects" in config:
            print("Error: No group or project configured. Stopping.")
            exit(1)
        else:
            projects = config["projects"]

merge_requests = get_project_mrs(gl, projects)
# project_objects = get_project_settings(gl, projects)
fields = merge_requests[0].keys()
write_csv(merge_requests, fields)

mytemplate = Template(filename='template/index.html')

with open("public/index.html","w") as outfile:
    #try:
    outfile.write(mytemplate.render(projects = merge_requests, fields = fields))
    #except:
    #    print("Could not render HTML")